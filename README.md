# CC422 - PC2

**THEME:** Multi-AZ DB Cluster - AWS RDS (engine: Aurora-MySQL)

In this homework, we have deploy the following infrastructure:
![Multi-AZ DB cluster](./images/infrastructure.png)


# QuickStart
```sh
# run bash script to deploy infrastructure
$ bash rds.sh
```