#!/bin/bash
#
# Course: CC442 Infraestructura de Computacion - 20231
# Student: Lozano Gustavo - 20161317K
#
# NOTE: 
# This script was executed with AWS CLI v2 (there may be incompatibilities with AWS CLI v1)
#
# IMPORTANT:
# - When you create a DB Cluster using Amazon Console (WEB), it automaticaly create primary instance
#   for your cluster but with AWS CLI, it doesn't, 
#   so we need to create it manually (best for us - more to learn!!!)
#
# 
# This script deploy an database cluster on multi-AZ
# - cluster architecture: single master node and 2 instances [1 read replicate (on a different AZ)]
# - engine: aurora-mysql
# - engine-
# - default vpc and subnet
# - security goup to allow access to port 3306
# - public accesible
# - authentication: user-password
# - enable encryption
# - enable monitorization
# - enable log (errors)
# - enable deletion protection
# - backups : ?
#
# References:
# - How can I create a multi-AZ database cluster?
# |- https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Aurora.CreateInstance.html
# |
# - What kind of instances can I choice to deploy my database cluster?
# |- https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Concepts.DBInstanceClass.html
# |
# - How can I connect me to my DB cluster?
# |- https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Aurora.Connecting.html
# |
# -



# Reading options (DB Cluster name and DB name)
if [[ $# -lt 1 ]]; then
  echo "USAGE: $0 DB_CLUSTER_NAME"
  exit 1
fi

DB_CLUSTER_NAME=$1
echo "[+] DB cluster name: $DB_CLUSTER_NAME"


# networks
defaultVpc=$(aws ec2 describe-vpcs \
                --query 'Vpcs[?IsDefault==`true`].VpcId' \
                --output text)
echo "[*] VPC: $defaultVpc"

subnetsId=$(aws ec2 describe-subnets \
              --filter "Name=vpc-id,Values=$defaultVpc" \
              --query "Subnets[].SubnetId" \
              --output text)

echo "[*] Subnets: $subnetsId"


# DB security group (port: 3306, engine: aurora-mysql)
DB_SG_NAME='db_sg'
DB_SG_DESCRIPTION='DB-Security-Group'

db_sg=$(aws ec2 describe-security-groups \
          --group-names $DB_SG_NAME 2> /dev/null \
          | grep GroupId | cut -d '"' -f4)

if [[ -z $db_sg ]]; then
  echo "Creating DB security group"
  db_sg=$(aws ec2 create-security-group \
            --description $DB_SG_DESCRIPTION  \
            --group-name $DB_SG_NAME \
            --vpc-id $defaultVpc  \
            --tag-specifications "ResourceType=security-group,Tags=[{Key=Name, Value=$DB_SG_NAME}]" \
            | grep GroupId | cut -d '"' -f4)
fi

echo "[*] DB security group: $db_sg"


# Adding ingress to DB security group
echo "[*] Adding 3306/tcp from 0.0.0.0/0 ingress to DB security group"
aws ec2 authorize-security-group-ingress \
  --group-id $db_sg \
  --protocol tcp \
  --port 3306 \
  --cidr 0.0.0.0/0 2>/dev/null



# DB subnet group
DB_SUBNET_GROUP_NAME='mydbsubnet'
DB_SUBNET_GROUP_DESCRIPTION='Default-DB-SubnetGroup'
dbSubnetGroupName=$(aws rds describe-db-subnet-groups \
                      --db-subnet-group-name $DB_SUBNET_GROUP_NAME \
                      --query 'DBSubnetGroups[*].DBSubnetGroupName' \
                      --output text 2> /dev/null)

if [[ -z $dbSubnetGroupName ]]; then
  echo "[+] Creating DB Subnet Group $DB_SUBNET_GROUP_NAME"
  aws rds create-db-subnet-group \
    --db-subnet-group-name $DB_SUBNET_GROUP_NAME \
    --db-subnet-group-description $DB_SUBNET_GROUP_DESCRIPTION \
    --subnet-ids $subnetsId
fi


# getting credentials for DB cluster

echo "[*] Creating credentials for Database"
echo -n "User: "
read user
echo -n "Password: "
read -s password
echo -e -n "\nConfirm your password: "
read -s confirmPassword

if [[ $password != $confirmPassword ]];then
  echo "Confirmation password is different!"
  exit 1
fi


# Creating DB cluster (using aurora-mysql engine v5.7)
echo "[*] Creating $DB_CLUSTER_NAME multi-AZ db cluster"

DB_ENGINE='aurora-mysql'
DB_ENGINE_VERSION=5.7
DB_CLUSTER_INSTANCES_TYPE=db.r5.large # type of instances in DB cluster 

aws rds create-db-cluster \
  --engine $DB_ENGINE \
  --engine-version $DB_ENGINE_VERSION \
  --db-cluster-identifier $DB_CLUSTER_NAME \
  --db-subnet-group-name $DB_SUBNET_GROUP_NAME \
  --vpc-security-group-ids $db_sg \
  --master-username $user \
  --master-user-password $password \
  --storage-encrypted \
  --enable-cloudwatch-logs-exports error \
  --deletion-protection \
  --no-paginate --no-cli-pager \
  | grep -E "DBClusterIdentifier|DBSubnetGroup|EngineVersion|Port"
  

# Creating instances for DB cluster
INSTANCES=2
for i in $(seq $INSTANCES);do
  instanceName="$DB_CLUSTER_NAME-instance-$i"
  
  echo "[+] Creating instance $instanceName for DB cluster"
  aws rds create-db-instance \
    --db-instance-identifier $instanceName  \
    --db-cluster-identifier $DB_CLUSTER_NAME \
    --engine $DB_ENGINE \
    --db-instance-class $DB_CLUSTER_INSTANCES_TYPE \
    --no-paginate --no-cli-pager \
    | grep -E "DBInstanceIdentifier|DBInstanceClass|EngineVersion|PubliclyAccessible"
done

# PROBLEM WITH OPTION (correct value for instance-type?)
# --db-cluster-instance-class $DB_CLUSTER_INSTANCES_TYPE \

# INVALID OPTIONS FOR aurora-mysql
#--monitoring-interval 60 \
# --publicly-accessible <> PubliclyAccessible isn't supported for DB en gine aurora-mysql
# --allocated-storage 10 \

#echo "Waiting for $DB_CLUSTER_NAME DB cluster creation ..."
#aws rds describe-db-clusters \
#  --db-cluster-identifier $dbClusterIdentifier \
#  --filter 'Name=endpoint_type,Values=DescribeDBClusterEndpoints,DescribeDBInstances'